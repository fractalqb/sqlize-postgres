module git.fractalqb.de/fractalqb/sqzpg

go 1.22.0

toolchain go1.23.2

require git.fractalqb.de/fractalqb/sqlize v0.10.0

require (
	github.com/jackc/pgpassfile v1.0.0 // indirect
	github.com/jackc/pgservicefile v0.0.0-20240606120523-5a60cdf6a761 // indirect
	github.com/jackc/puddle/v2 v2.2.2 // indirect
	golang.org/x/sync v0.10.0 // indirect
)

require (
	github.com/jackc/pgx/v5 v5.7.2
	github.com/mattn/go-sqlite3 v1.14.24 // indirect
	github.com/ngrok/sqlmw v0.0.0-20220520173518-97c9c04efc79 // indirect
	golang.org/x/crypto v0.32.0 // indirect
	golang.org/x/exp v0.0.0-20250106191152-7588d65b2ba8 // indirect
	golang.org/x/text v0.21.0 // indirect
)
