package sqzpg

import (
	"database/sql"
	"errors"
	"fmt"
	"testing"
	"time"

	"git.fractalqb.de/fractalqb/sqlize"
	"git.fractalqb.de/fractalqb/sqlize/bsq"
	. "git.fractalqb.de/fractalqb/sqlize/bsq/keywords"
	"git.fractalqb.de/fractalqb/sqlize/dbtest"
	"git.fractalqb.de/fractalqb/sqlize/entity"
	"git.fractalqb.de/fractalqb/sqlize/examples"
	pgx "github.com/jackc/pgx/v5/stdlib"
)

func ExampleQuery() {
	tbl := struct {
		bsq.Table
		ID, Name bsq.Column
	}{Table: bsq.Table{Name: "test", DefaultAlias: "t"}}
	bsq.InitSchema("", nil, &tbl)
	als := tbl.As("t2")

	q, err := bsq.NewQuery(Dialect,
		SELECT, bsq.Out(&tbl.ID, tbl.Name.In(als)),
		FROM, bsq.Ls(", ").Of(&tbl, als),
		WHERE, bsq.P("name"), IS_NOT_NULL,
		AND, bsq.PGe(&tbl.ID), AND, bsq.PLt(&tbl.ID),
	)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(q.String())
	// Output:
	// SELECT t.ID, t2.Name FROM test t, test t2 WHERE $1 IS NOT NULL AND t.ID>=$2 AND t.ID<$2
}

func TestRepo(t *testing.T) {
	// Create user repository
	userRepo, err := examples.NewUserDBRepo(Dialect)
	if err != nil {
		t.Fatal(err)
	}

	// Open an in memory DB with logging
	sqlize.LogDriver(
		t.Name(), &pgx.Driver{},
		dbtest.NewLogger(t), sqlize.LogAll,
	)
	db, err := sql.Open(t.Name(), dbtest.SourceName(t, "host=localhost"))
	if err != nil {
		t.Fatal(err)
	}
	defer db.Close()

	// Setup DB schema
	sqlize.RunScriptFile(db, "testdata/drop.sql", true)
	if err := sqlize.RunScriptFile(db, "testdata/create.sql", true); err != nil {
		t.Fatal(err)
	}

	dbtest.CRUDTest[examples.User, entity.ID32]{
		Repo: userRepo,
		Create: &examples.User{
			Name:    "John Doe",
			RegDate: time.Date(1977, time.May, 12, 17, 33, 06, 0, time.UTC).Round(time.Second),
		},
		Read: new(examples.User),
		Equal: func(u, v *examples.User) error {
			if !u.Equal(v) {
				return errors.New("not equal")
			}
			return nil
		},
	}.
		Update("Name", func(e *examples.User) { e.Name = "Max Mustermann" }).
		Run(t, db)
}
