package sqzpg

import (
	"git.fractalqb.de/fractalqb/sqlize/bsq"
	. "git.fractalqb.de/fractalqb/sqlize/bsq/keywords"
)

type Upsert bsq.Upsert

func (ups Upsert) Defn() []any {
	if err := (*bsq.Upsert)(&ups).Complete(); err != nil {
		return nil
	}
	allCols := ups.Columns.Defn(ups.Key.Defn(nil))
	allParam := make([]any, len(allCols))
	for i, c := range allCols {
		allParam[i] = bsq.ColP(bsq.P(c))
	}
	res := []any{INSERT_INTO, ups.Table, ' ',
		bsq.Ls("(", ", ", ")").Of(allCols...),
		VALUES, bsq.Ls("(", ", ", ")").Of(allParam...),
		" ON CONFLICT ", bsq.Ls("(", ", ", ")").Of(ups.Key.Defn(nil)...),
		" DO UPDATE SET ",
	}
	for i, c := range ups.Columns {
		if i > 0 {
			res = append(res, ", ")
		}
		res = append(res, bsq.PEq(c))
	}
	return res
}
