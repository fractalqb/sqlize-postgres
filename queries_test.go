package sqzpg

import (
	"fmt"

	"git.fractalqb.de/fractalqb/sqlize/bsq"
	"git.fractalqb.de/fractalqb/sqlize/examples"
)

func ExampleUpsert() {
	ups, err := bsq.NewQuery(Dialect, Upsert{Key: bsq.Columns{&examples.TUsers.ID}}.Defn())
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(ups.String())
	// Output:
	// INSERT INTO users (id, name, reg_date) VALUES ($1, $2, $3) ON CONFLICT (id) DO UPDATE SET name=$2, reg_date=$3
}

func ExampleDialect_DefCreate() {
	cre := Dialect.DefCreate(&examples.TUsers.ID)
	if err := cre.Init(Dialect); err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(cre.String())
	// Output:
	// INSERT INTO users (name, reg_date) VALUES ($1, $2) RETURNING id
}
