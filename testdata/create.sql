--*- mode: sql; sql-product: sqlite; -*-
CREATE TABLE users (
  id serial PRIMARY KEY
, name text NOT NULL UNIQUE
, reg_date timestamptz NOT NULL
);

CREATE TABLE groups (
  id serial PRIMARY KEY
, name text NOT NULL UNIQUE
);

CREATE TABLE membership (
  "user" integer REFERENCES users(id)
, "group" integer REFERENCES groups(id)
, PRIMARY KEY ("user", "group")
);
