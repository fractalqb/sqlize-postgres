package sqzpg

import (
	"context"
	"fmt"
	"strings"

	"git.fractalqb.de/fractalqb/sqlize"
	"git.fractalqb.de/fractalqb/sqlize/bsq"
)

var (
	Dialect dialect

	_ bsq.Dialect       = Dialect
	_ bsq.UpsertDialect = Dialect
	_ bsq.CreateDialect = Dialect
)

type dialect struct{}

func (dialect) NewContext() bsq.DialectCtx {
	return &bsqCtx{
		IndexBindVars: *bsq.NewIndexBindVars("$%d"),
	}
}

func (dialect) Quote(name string) string {
	if PGkeywords[strings.ToUpper(name)] {
		var sb strings.Builder
		sb.WriteByte('"')
		sb.WriteString(name)
		sb.WriteByte('"')
		return sb.String()
	}
	return name
}

func (dialect) Parameter(ctx bsq.DialectCtx, name string, col *bsq.Column) (string, error) {
	c := ctx.(*bsqCtx)
	return c.Param(name, col)
}

type bsqCtx struct {
	bsq.WriteCtx
	bsq.IndexBindVars
}

func (*bsqCtx) Close() {}

func (d dialect) DefUpsert(ups bsq.Upsert) *bsq.Query {
	return bsq.DefQuery(Upsert(ups))
}

func (d dialect) DefCreate(idColumn *bsq.Column, cols ...*bsq.Column) *bsq.Query {
	var ins bsq.Insert
	if cols == nil {
		ins.Columns = bsq.Cols(idColumn.Table(), idColumn)
	} else {
		ins.Columns = cols
	}
	defn := ins.Defn()
	defn = append(defn, " RETURNING ", idColumn)
	return bsq.DefQuery(defn)
}

func (dialect) QueryCreateContext(ctx context.Context, db sqlize.Querier, q *bsq.Query, args ...any) (id int64, err error) {
	err = db.QueryRow(q.String(), args...).Scan(&id)
	return
}

func (dialect) SQLSelect1Row(table string) string {
	return fmt.Sprintf("SELECT * FROM %s LIMIT 1", table)
}
